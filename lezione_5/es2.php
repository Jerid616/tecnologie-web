<html>
	<head>
		<title> Esercizio 2 </title>
		<style>
			table, th, td {
				color:green;
				font-size:20pt;
				border-width:1;
				border:solid;
				text-align:center;
			}
		</style>
	</head>
	<body>
		<?php
			$rows = array();
			$data = array('Nome' => 'Mario', 'Cognome' => 'Rossi', 'Salario' => '20.000');
			array_push($rows, $data);
			$data = array('Nome' => 'Alberto', 'Cognome' => 'Bianchi', 'Salario' => '21.000');
			array_push($rows, $data);
			$data = array('Nome' => 'Giulio', 'Cognome' => 'Neri', 'Salario' => '26.000');
			array_push($rows, $data);
  		?>
		<table border="1">
			<tr>
                <td>NOME</td>
                <td>COGNOME</td>
				<td>SALARIO</td>
			</tr>
			<?php for($i=0; $i<count($rows); $i++): ?>
			<tr>
                <th><?php echo $rows[$i]['Nome']; ?></th>
                <th><?php echo $rows[$i]['Cognome']; ?></th>
                 <th><?php echo $rows[$i]['Salario']; ?></th>
			</tr>
			<?php endfor; ?>
        </table>
	</body>
</html>
