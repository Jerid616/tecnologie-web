<?php
	class Person {
		// proprietà
		public $first_name;
		public $last_name;
	
		// costruttore
		public function __construct($fn, $ln) {
			$this->first_name = $fn; 
			$this->last_name = $ln;
		}
	
		// metodi	
		public function setFirstName($fn) {$this->first_name = $fn;}
		public function setLastName($ln) {$this->last_name = $ln;}
		public function getFirstName() {return $this->first_name;}
		public function getLastName() {return $this->last_name;}
		public function getFullName() {return $this->first_name ." ". $this->last_name;}
	}
?>

<html>
	<head>
		<title> Esercizio 3 </title>
		<style>
			input[type="text"] {
				border: 2px solid red;
    			border-radius: 4px;
			}

			input[type=text]:focus {background-color: lightblue;}
		</style>
	</head>
	<body>
		    <body>
        <?php
            if(!empty($_POST)) {
				$p=new Person($_POST["nome"], $_POST["cognome"]);
				echo $p->getFullName()."\n";
            }
        ?>
        <form action="es3.php" method="post">
       	    <p>
       	        Nome:<input type="text" name="nome"/> 
       	    </p>
       	    <p>
       	       Cognome:<input type="text" name="cognome"/> 
       	    </p>
       	    <p>
       	    	<input type="submit" name="send"/>
        	</p>
        </form>
    </body>
</html>
