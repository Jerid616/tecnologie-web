<?php 
	class Prodotto {
		private $name, $price;
		public function __construct($n, $p) {
			$this->name=$n; //ricordare sempre il $this quando si adoperano le variabili nella classe!
			$this->price=$p;
		}

		public function getName() {return $this->name;} //i metodi non vogliono il $ davanti!
		public function getPrice($valuta) {return $this->price." ".$valuta;} //LA CONCATENAZIONE IN PHP SI FA COL PUNTO!
		public function setName($n) {$this->name=$n;}
		public function setPrice($p) {$this->price=$p;}
	}

	$p1=new Prodotto("Nome 1",'10.00');
	$p2=new Prodotto("Nome 2",'12.00');
	$p3=new Prodotto("Nome 3",'16.00');
	$p4=new Prodotto("Nome 4",'43.00');
	$p5=new Prodotto("Nome 5",'5.00');
	$p6=new Prodotto("Nome 6",'22.00');
	$p7=new Prodotto("Nome 7",'18.00');
	$p8=new Prodotto("Nome 8",'13.00');
	$p9=new Prodotto("Nome 9",'10.00');
	$p10=new Prodotto("Nome 10",'9.00');
	$p11=new Prodotto("Nome 11",'11.00');
	$p12=new Prodotto("Nome 12",'18.00');
	$p13=new Prodotto("Nome 13",'14.00');
	$p14=new Prodotto("Nome 14",'30.00');
	$p15=new Prodotto("Nome 15",'33.00');
	$data = array($p1, $p2, $p3, $p4, $p5, $p6, $p7, $p8, $p9, $p10, $p11, $p12, $p13, $p14, $p15); //per creare un array non ci vuole la parola chiave new come invece in Java!
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Bootstrap Template</title>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	
		<style>
			.row { 
				margin-top: 50px; 
				margin-bottom: 50px; 
			}
		</style>
	</head>
	<body>
		<div class="container">
			<h1>Esercizio nr 3</h1>

			<div class="row">
				<?php for($i=0; $i<count($data); $i++): ?>
					<div class="col-sm-3">
						<div class="prodotto text-center">
							<img src='http://placehold.it/200x200.jpg'/>
							<h4><?php echo $data[$i]->getName() ?></h4>
							<h6><?php echo $data[$i]->getPrice('€') ?></h6>
						</div>			
					</div>

				<?php if ($i % 4 == 3): ?>
				</div> <!-- chiudo la row -->
				<div class="row"> <!-- apro una nuova row -->
				<?php endif; ?>

				<?php endfor; ?>
			</div> <!-- chiudo l'ultima rows -->

		</div>
	</body>
</html>